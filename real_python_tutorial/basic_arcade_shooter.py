"""
Basic arcade shooter
"""

# Imports
import arcade
import random
import time

# Constants
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_TITLE = "Arcade Space Shooter"
SCALING = 1.25


# Classes


class SpaceShooter(arcade.Window):
    def __init__(self, width, height, title):
        super().__init__(width, height, title)

        self.paused = False

        # Set up the empty sprite lists
        self.enemies_list = arcade.SpriteList()
        self.clouds_list = arcade.SpriteList()
        self.all_sprites = arcade.SpriteList()

    def setup(self):

        # Set the background color
        arcade.set_background_color(arcade.color.SKY_BLUE)

        # Set up the player
        self.player = arcade.Sprite(f"assets/ship_{random.randint(0, 11):04d}.png", SCALING)
        self.player.center_y = self.height / 2
        self.player.left = 10
        self.player.angle = -90
        self.all_sprites.append(self.player)

        # Spawn a new enemy every 0.25 seconds
        arcade.schedule(self.add_enemy, 0.25)

        # Load sounds
        self.collision_sound = arcade.load_sound("assets/explosionCrunch_002.ogg")
        self.move_up_sound = arcade.load_sound("assets/highUp.ogg")
        self.move_down_sound = arcade.load_sound("assets/highDown.ogg")

    def add_enemy(self, delta_time):

        # First, create a new enemy sprite
        enemy = FlyingSprite(f"assets/ship_00{random.randint(12, 23)}.png", SCALING)

        # Set its position to a randow height and off screen right
        enemy.left = random.randint(self.width, self.width + 80)
        enemy.top = random.randint(10, self.height - 10)
        enemy.angle = 90

        # Set its speed to a random speed heading left
        enemy.velocity = (random.randint(-300, -100), 0)

        # Add it to the enemies list
        self.enemies_list.append(enemy)
        self.all_sprites.append(enemy)

    def on_key_press(self, symbol, modifiers):

        if symbol == arcade.key.Q:
            arcade.close_window()

        if symbol in [arcade.key.I, arcade.key.UP]:

            self.player.change_y = 200
            arcade.play_sound(self.move_up_sound)

        if symbol in [arcade.key.K, arcade.key.DOWN]:

            self.player.change_y = -200
            arcade.play_sound(self.move_down_sound)

        if symbol in [arcade.key.J, arcade.key.LEFT]:

            self.player.change_x = -200

        if symbol in [arcade.key.L, arcade.key.RIGHT]:

            self.player.change_x = 200

    def on_key_release(self, symbol, modifiers):

        if symbol in [arcade.key.I, arcade.key.K, arcade.key.UP, arcade.key.DOWN]:

            self.player.change_y = 0

        if symbol in [arcade.key.J, arcade.key.L, arcade.key.LEFT, arcade.key.RIGHT]:

            self.player.change_x = 0

    def on_update(self, delta_time):

        # If paused, don't update anything
        if self.paused:
            return

        # Colission
        if self.player.collides_with_list(self.enemies_list):
            arcade.play_sound(self.collision_sound)
            time.sleep(1)
            print("GAME OVER")
            arcade.close_window()

        # Update everything
        for sprite in self.all_sprites:
            sprite.center_x = int(sprite.center_x + sprite.change_x * delta_time)
            sprite.center_y = int(sprite.center_y + sprite.change_y * delta_time)

        # Keep the player on screen
        self.player.top = min(self.player.top, self.height)
        self.player.bottom = max(self.player.bottom, 0)
        self.player.right = min(self.player.right, self.width)
        self.player.left = max(self.player.left, 0)

    def on_draw(self):

        arcade.start_render()
        self.all_sprites.draw()


class FlyingSprite(arcade.Sprite):
    """Base class for all flying sprites

    Flying sprites include enemies and clouds
    """

    def update(self):

        # Move the sprite
        super().update()

        # Remove if off the screen
        if self.right < 0:
            self.remove_from_sprite_lists()


# Main code entry point
if __name__ == "__main__":
    app = SpaceShooter(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    app.setup()
    arcade.run()
